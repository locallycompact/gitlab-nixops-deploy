{
  packageOverrides = pkgs: {
    gitlab = pkgs.gitlab.override {
      src = fetchFromGitLab {
        owner = "gitlab-org";
        repo = "gitlab-ce";
        rev = "v11.7.1";
        sha256 = "0bbyx9zmscf9273fgypb82gw166psy7d3p7dnwb6f5r9yz7rmhbn";
      };
     };
  };
}
